export class UpdateUserEvent {
  constructor(
      public readonly email: string,
      public readonly password:string,
      public readonly requesting_id: number) {}
}