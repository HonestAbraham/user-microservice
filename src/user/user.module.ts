import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entities/User';
import { Blog } from 'src/entities/blog.entity';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forFeature([User, Blog]), ConfigModule.forRoot({})],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService]
})
export class UserModule {}
