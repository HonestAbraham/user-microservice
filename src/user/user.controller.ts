import { Controller, Query, Req } from '@nestjs/common';
import { UserService } from './user.service';
import { EventPattern, MessagePattern } from '@nestjs/microservices';
import { UserOptionDto } from 'src/dto/UserOption.dto';
import { UpdateUserDto } from 'src/dto/UpdateUser.dto';
import { firstValueFrom } from 'rxjs';
import { UserRole } from 'src/util/UserRole';

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {

    }


    @MessagePattern({ cmd: 'get_all_users' })
    // @MessagePattern('createUser')
    async getAllUsers(userOptionsDto) {
        const response = await this.userService.fetchUsers(UserRole.Admin, userOptionsDto)
        console.log("test 1")
        console.log(response)
        return response
    }

    @MessagePattern({ cmd: 'get_limited_users' })
    async getLimitedUsers(userOptionsDto) {
        console.log("test 2")
        const response = await this.userService.fetchUsers(UserRole.Member, userOptionsDto) 
        console.log(response)
        return response;
    }

    @MessagePattern({ cmd: 'find_by_id' })
    findById(id) {
        return this.userService.getUserById(id);
    }

    @MessagePattern({ cmd: 'find_by_username' })
    findByUsername(username){
        return this.userService.findOneByUsername(username);
    }

    @EventPattern('update_this_user')
    updateThisUser(payload) {
        console.log("user updateThisUser - controller")
        console.log(payload)
        this.userService.update_my_user(payload.id, payload.updateUserDto)
    }

    @EventPattern('delete_this_user')
    deleteThisUser(payload) {
        console.log("user deleteThisUser - controller")
        console.log(payload)
        this.userService.delete_my_user(payload.id)
    }
}
