import { ForbiddenException, HttpException, HttpStatus, Injectable, Query, Req } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateUserDto } from 'src/dto/UpdateUser.dto';
import { UserOptionDto } from 'src/dto/UserOption.dto';
import { User } from 'src/entities/User';
import { Blog } from 'src/entities/blog.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UserRole } from 'src/util/UserRole';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(User) private userRepo: Repository<User>,
        @InjectRepository(Blog) private blogRepo: Repository<Blog>,
        private config: ConfigService,
    ) { }


    async fetchUsers(role: UserRole, userOptionsDto: UserOptionDto) {
        // console.log(role)
        let users;
        if (role == UserRole.Admin) {
            users = await this.userRepo.find({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                take: userOptionsDto.limit,
                skip: (userOptionsDto.page - 1) * userOptionsDto.limit,
                order: { id: userOptionsDto.orderBy }
            });
            // console.log("test")
            // console.log(users)


        } else {
            users = await this.userRepo.find({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                where: {
                    role: UserRole.Member
                },
                skip: (userOptionsDto.page - 1) * userOptionsDto.limit,
                take: userOptionsDto.limit,
                order: { id: userOptionsDto.orderBy }

            });

        }
        console.log(users)
        return users;
    }

    async getUserById(userId: number) {
        try {
            const user = await this.userRepo.findOne({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                where: {
                    id: userId
                }
            });

            if (!user) {
                throw new HttpException("Could not find User", HttpStatus.BAD_REQUEST);
            }

            return user;
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }

    }

    async findOneByUsername(username: string) {
        try {
            const user = await this.userRepo.findOneOrFail({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                where: {
                    username: username
                }
            });
            return user;
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }

    }


    async update_my_user(id: number, updateUserDto: UpdateUserDto) {
        try{
            const user = await this.userRepo.findOne({
                where: {
                    email: updateUserDto.email
                }
            })
    
            if (user) {
                throw new ForbiddenException("email: " + updateUserDto.email + " is already taken")
            }
        }catch(error){
            console.log("Duplicate Entry")
        }
        

        try {
            const hashedPassword = await bcrypt.hashSync(updateUserDto.password, parseInt(this.config.get<string>("SALT_ROUNDS")));

            updateUserDto.password = hashedPassword;
            this.userRepo.update({ id: id }, { ...updateUserDto });
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }
    }

    async delete_my_user(id: number) {

        try {
            const user = await this.userRepo.findOne({ where: { id: id }, relations: ['blogs'] })
            for (const blog of user.blogs) {
                await this.blogRepo.softDelete(blog.id);
            }
            this.userRepo.softDelete({ id: id });
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST)
        }
    }
}
