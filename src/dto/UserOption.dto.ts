import { ApiProperty } from "@nestjs/swagger";
import { Transform } from "class-transformer";
import { IsEnum, IsInt, IsNotEmpty, Min, min } from "class-validator";
import { Order } from "src/util/OutputOrder";



export class UserOptionDto{
    
    @Min(1)
    @ApiProperty({
        description: "Number of Users to Get",
        example: 2
    })
    @IsNotEmpty()
    @Transform(({ value }) => parseInt(value))
    @IsInt()
    limit: number = 1;

    @Min(1)
    @ApiProperty({
        description: "Page Number",
        example: 1
    })
    @Transform(({ value }) => parseInt(value))
    @IsInt()
    @IsNotEmpty()
    page: number = 1;


    @ApiProperty({
        description: "Order By",
        example: "ASC"
    })
    @IsEnum(Order)
    @IsNotEmpty()
    orderBy: Order;
}