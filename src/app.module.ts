import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { TypeOrmModule } from '@nestjs/typeorm'
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { User } from './entities/User';
import { Blog } from './entities/blog.entity';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';





@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([User, Blog]),
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: (configService: ConfigService) => ({
      type: 'mysql',
      host: configService.get<string>('DATABASE_HOST'),
      port: parseInt(configService.get<string>('DATABASE_PORT')),
      username: configService.get<string>('DATABASE_USER'),
      password: configService.get<string>('DATABASE_PASS'),
      database: configService.get<string>('DATABASE_NAME'),
      entities: [User, Blog],
      synchronize: false,
      logging: ["error", "query"]
    }),
    inject: [ConfigService],
  }),
    UserModule],
  controllers: [AppController, UserController],
  providers: [AppService, UserService],
})

export class AppModule { }
